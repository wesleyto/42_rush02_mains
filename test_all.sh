NUM_EXAMPLES=32

if ([ "$#" -eq 1 ] && [ $1 == '-sq' ]) || ([ "$#" -eq 2 ] && [ $2 == '-sq' ]);
then
	EXPECTED_FOLDER=EXPECTED_SQUARE
else
	EXPECTED_FOLDER=EXPECTED
fi

### NORMINETTE ###
if ([ "$#" -eq 1 ] && [ $1 == '-n' ]) || ([ "$#" -eq 2 ] && [ $2 == '-n' ]);
then
	echo ===========================
	echo === Skipping Norminette ===
	echo ===========================
else
	echo ===========================
	echo ======= Norminette ========
	echo ===========================
	norminette ex00/*
fi
echo ...Done
echo

### COMPILING FILES ###
echo ===========================
echo ===== Test Compiling ======
echo ===========================
gcc -Wall -Wextra -Werror -c ex00/*.c
rm *.o
echo ...Done
echo

### COPY OVER TEST FILES ###
echo ===========================
echo ====== Copying Files ======
echo ===========================
cp -R ex00/* TESTFILES/ex00/
echo ...Done
echo

### COMPILE WITH TEST FILES ###
echo ===========================
echo ===== Compiling Mains =====
echo ===========================
cd TESTFILES/ex00
make all
make clean
echo ...Done
echo

rm -rf OUTPUTS
rm -rf RESULTS
mkdir OUTPUTS
mkdir RESULTS

### RUN TEST FILES ###
echo ===========================
echo ==== Testing Examples =====
echo ===========================
for num in $(seq -f "%02g" 0 $NUM_EXAMPLES)
do
	cat EXAMPLES/example$num | ./rush-2 > OUTPUTS/out_ex$num
	diff OUTPUTS/out_ex$num $EXPECTED_FOLDER/exp_ex$num > RESULTS/diff_ex$num
		if [ -s RESULTS/diff_ex$num ]; then
		echo "==>Example $num: Failure"
		cat RESULTS/diff_ex$num
	else
		echo "Example $num: Success"
	fi
done
echo
make fclean
cd ..
cd ..