# README #

### What is this repository for? ###

* 42 rush02 testing

### Notes ###

* These are my interpretations of the instructions.
* Moulinette can always vary from these test, obviously.
* These tests just serve as sanity checks.
* You should still examine code to check that the other instructions were followed.
* NO GUARANTEES. I have found and corrected mistakes in these test files myself, and there may be more, or there may be missing test cases.
* Always examine the test cases and the results, and interpret them for yourself.
* This speeds up the evaluation process, but does not replace it. Don't rely on it.

### How do I get set up? ###

* Move the ***TESTFILES/*** directory and ***test_all.sh*** script to your project directory.
* You *don't* have to copy/merge any other files/dirs over. The script does it for you.
* Run the script to check norm, compile, and run the test cases.
* Run the script with the ***-n*** flag to skip norminette
* Run the script with the ***-sq*** flag to check with *square (carre)* testing
* All the auxiliary files are contained in ***TESTFILES/***, so delete that directory and replace it with a fresh copy to start over.

### Who do I talk to? ###

* Repo owner